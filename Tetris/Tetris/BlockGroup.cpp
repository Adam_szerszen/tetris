﻿#include "BlockGroup.h"

Color* BlockGroup::getColor() const
{
	return blockGroupColor;
}

BlockGroup::BlockGroup(Color* blockGroupColor, float blockWidth, float blockHeight) :
	blockGroupColor(blockGroupColor),
	blockWidth(blockWidth),
	blockHeight(blockHeight)
{
	blocksInside = new vector<Block*>();
}

BlockGroup::~BlockGroup()
{
}
