﻿#include "Block.h"

Block::Block(Color* blockColor, float width, float height) :
	width(width),
	height(height),
	blockColor(blockColor),
	blockFrame(nullptr)
{
	blockFrame = new RectangleShape(Vector2f(this->width, this->height));
	blockFrame->setFillColor(*this->blockColor);
	blockFrame->setOutlineThickness(1.0);
	blockFrame->setOutlineColor(Color::Black);
	blockFrame->setPosition(0.0, 0.0);
}

Block::~Block()
{
	delete blockColor;
}
