﻿#pragma once
#include <vector>
#include "Block.h"

using std::vector;

class BlockGroup
{
private:
	Color* blockGroupColor;

protected:
	float blockWidth;
	float blockHeight;
	vector<Block*>* blocksInside;

	Color* getColor()const;
	virtual void CreateGrup() = 0;

public:
	explicit BlockGroup(Color* blockGroupColor, float blockWidth, float blockHeight);
	virtual ~BlockGroup();
};
