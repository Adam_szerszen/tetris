﻿#pragma once
#include <SFML/Graphics.hpp>

using sf::Color;
using sf::RectangleShape;
using sf::Vector2f;

class Block
{
private:
	float width;
	float height;

	Color* blockColor;
	RectangleShape* blockFrame;
	
public:
	explicit Block(Color* blockColor, float width, float height);
	~Block();
};
